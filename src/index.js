import MapLibre from "maplibre-gl";

import VueMap from "./components/VueMap.vue";
import VueMapMarkers from "./components/VueMapMarkers.vue";
import VueMapPolygon from "./components/VueMapPolygon.vue";
import VueMapHereControl from "@/components/VueMapHereControl.vue";
import VueMapZoomControl from "@/components/VueMapZoomControl.vue";
import VueMapScaleControl from "@/components/VueMapScaleControl.vue";
import VueMapFullscreenControl from "@/components/VueMapFullscreenControl.vue";
import VueMapCustomHere from "./components/VueMapCustomHere.vue";
import VueMapMarker from "./components/VueMapMarker.vue";
import VueMapCluster from "./components/VueMapCluster.vue";

export {
	MapLibre,
	VueMap,
	VueMapMarkers,
	VueMapPolygon,
	VueMapHereControl,
	VueMapZoomControl,
	VueMapScaleControl,
	VueMapFullscreenControl,
	VueMapCustomHere,
	VueMapMarker,
	VueMapCluster
};

export { useMapStore } from "./stores/map.js";
export { useAttachable } from "./composables/attachable.js";
export { useGeolocation } from "./composables/geolocation.js";
export { useMapMarker, propsDefinition as commonMarkerProperties } from "./composables/map-marker.js";
