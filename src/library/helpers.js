/**
 * Validates if provided value is a LngLatLike value unless nullish.
 *
 * @param {Object|Array} value value to check
 * @returns {boolean|null} null if value is nullish, true if non-nullish value is a LngLatLike
 */
export function isLngLatLike( value ) {
	if ( value == null ) {
		return null;
	}

	if ( Array.isArray( value ) ) {
		return value.length === 2;
	}

	if ( value.lng != null && value.lat != null ) {
		return true;
	}

	if ( value.lon != null && value.lat != null ) {
		return true;
	}

	return false;
}

/**
 * Validates if provided value is a GeoJSON of type "Feature".
 *
 * @param {Object|Array} value value to check
 * @returns {boolean|null} null if value is nullish, true if non-nullish value is a GeoJSON of type "Feature"
 */
export function isGeoJsonFeature( value ) {
	if ( value == null ) {
		return null;
	}

	if ( value?.type !== "Feature" ) {
		return false;
	}

	if ( value.geometry?.type !== "Point" ) {
		return false;
	}

	if ( !value.geometry.coordinates ) {
		return false;
	}

	return Boolean( value.properties );
}

/**
 * Validates if provided value is a list of GeoJSON objects of type "Feature".
 *
 * @param {Object|Array} value value to check
 * @returns {boolean|null} null if value is nullish, true if non-nullish value is a list of GeoJSON objects of type "Feature"
 */
export function isGeoJsonFeatureList( value ) {
	if ( value == null ) {
		return null;
	}

	if ( !Array.isArray( value ) ) {
		return false;
	}

	for ( const item of value ) {
		if ( !isGeoJsonFeature( item ) ) {
			return false;
		}
	}

	return true;
}

/**
 * Validates if provided value is a valid zoom level unless nullish.
 *
 * @param {Object|Array} value value to check
 * @returns {boolean|null} null if value is nullish, true if non-nullish value is a zoom level
 */
export function isZoom( value ) {
	if ( value == null ) {
		return null;
	}

	return !isNaN( Number( value ) ) && value >= 0 && value <= 24;
}

/**
 * Validates if provided value is a hex color code.
 *
 * @param {Object|Array} value value to check
 * @returns {boolean|null} null if value is nullish, true if non-nullish value is a hex color code
 */
export function isHexColorCode( value ) {
	if ( value == null ) {
		return null;
	}

	return typeof value === "string" && /^#?(?:[0-9a-f]{3}){1,2}$/.test( value );
}
