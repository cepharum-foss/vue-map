/**
 * This file implements custom functions available in terms used in shape
 * definitions for curing data. The same functions can be used by application,
 * too.
 */

/**
 * Calculates ID of map tile a given zoom-related geo-position is found.
 *
 * @param {number} longitude longitude of geo-position
 * @param {number} latitude latitude of geo-position
 * @param {number} zoomLevel zoom level
 * @returns {string} ID of tile containing given geo-position at selected zoom level
 */
export function tileatzoom( longitude, latitude, zoomLevel ) {
	const xAngle = longitude + 180;
	const yAngle = 90 - latitude;
	const numTiles = Math.pow( 2, zoomLevel );

	const x = Math.floor( xAngle / 360 * numTiles );
	const y = Math.floor( yAngle / 180 * numTiles ); // TODO consider numTiles/2 here

	return `${x}-${y}`;
}
