import { createRouter, createWebHistory } from "vue-router";

const router = createRouter( {
	history: createWebHistory( import.meta.env.BASE_URL ),
	routes: [
		{
			path: "/",
			name: "home",
			redirect: "/simple",
		},
		{
			path: "/simple",
			name: "simple",
			component: () => import( "@/views/SimpleView.vue" )
		},
		{
			path: "/alt",
			name: "alt",
			component: () => import( "@/views/AltView.vue" )
		},
		{
			path: "/mass-marker",
			name: "mass-marker",
			component: () => import( "@/views/MassMarkerView.vue" )
		},
		{
			path: "/route",
			name: "route",
			component: () => import( "@/views/RouteView.vue" )
		},
	]
} );

export default router;
