import { computed, ref } from "vue";

/**
 * Implements debouncing as a composable.
 *
 * @param {function} action callback invoked after some possibly repeating trigger has settled for given time
 * @param {number} time timespan in milliseconds a trigger has to settle prior to eventually invoking action callback
 * @returns {Object} provides API for triggering and observing a debounced action handler
 */
export function useDebounce( action, time ) {
	const timer = ref( null );

	return {
		/**
		 * Triggers pre-defined action handler unless this function is called
		 * again within pre-defined timespan.
		 *
		 * @returns {void}
		 */
		trigger: () => {
			clearTimeout( timer.value );

			timer.value = setTimeout( () => {
				timer.value = null;

				action();
			}, time );
		},

		/**
		 * Indicates if action has been triggered but hasn't been invoked yet
		 * due to debouncing.
		 *
		 * @type {ComputedRef<boolean>}
		 */
		pending: computed( () => timer.value != null ),
	};
}
