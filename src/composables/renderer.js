import { unref } from "vue";

/**
 * Generates renderer object for drawing animated pulsing dot on map. The object
 * is suitable to replace a loaded image in maplibre-gl API.
 *
 * @param {{size: Number}} props properties of consuming component
 * @param {Ref<Map>} map reactive API of map renderer is used with
 * @returns {StyleImageInterface} renderer for use with addImage() method of maplibre-gl
 */
export function usePulsingDotRenderer( props, map ) {
	const size = Number( unref( props ).size ) || 128;

	const normalizeColor = ( value, fallback = "#000000" ) => "#" + String( value || fallback )
		.replace( /^#/, "" )
		.replace( /^(.)(.)(.)$/, ( _, r, g, b ) => r + r + g + g + b + b );

	const hexCode = relativeCode => Math.floor( relativeCode * 256 ).toString( 16 ).padStart( 2, "0" );

	// see https://maplibre.org/maplibre-gl-js-docs/api/properties/#styleimageinterface
	// see https://maplibre.org/maplibre-gl-js-docs/example/add-image-animated/
	return {
		width: size,
		height: size,
		data: new Uint8Array( size * size * 4 ),

		onAdd() {
			const canvas = document.createElement( "canvas" );
			canvas.width = this.width;
			canvas.height = this.height;

			this.context = canvas.getContext( "2d", { willReadFrequently: true } );
		},

		render() {
			const duration = 1000;
			const t = ( performance.now() % duration ) / duration;

			const radius = ( size / 2 ) * 0.3;
			const outerRadius = ( ( size / 2 ) * 0.7 * t ) + radius;
			const context = this.context;

			const fill = normalizeColor( unref( props ).fill );
			const glow = normalizeColor( unref( props ).glow );
			const stroke = normalizeColor( unref( props ).stroke );
			const hexOpacity = hexCode( 1 - t );
			const animated = unref( props )?.animated ?? true;

			context.clearRect( 0, 0, this.width, this.height );

			if ( animated ) {
				// draw outer circle
				context.beginPath();
				context.arc(
					this.width / 2,
					this.height / 2,
					outerRadius,
					0,
					Math.PI * 2
				);
				context.fillStyle = glow + hexOpacity;
				context.fill();
			}

			// draw inner circle
			context.beginPath();
			context.arc(
				this.width / 2,
				this.height / 2,
				radius,
				0,
				Math.PI * 2
			);
			context.fillStyle = fill;
			context.strokeStyle = stroke;
			context.lineWidth = animated ? 2 + ( 4 * ( 1 - t ) ) : 6;
			context.fill();
			context.stroke();

			// update this image's data with data from the canvas
			this.data = context.getImageData(
				0,
				0,
				this.width,
				this.height
			).data;

			// continuously repaint the map, resulting in the smooth animation of the dot
			map.value?.triggerRepaint();

			// return `true` to let the map know that the image was updated
			return true;
		},
	};
}
