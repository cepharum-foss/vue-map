let nextId = 1;

export function useId( patterns = undefined ) {
	const id = nextId++;

	if ( patterns == null ) {
		return { id, ids: [id] };
	}

	if ( Array.isArray( patterns ) ) {
		return {
			id,
			ids: patterns.map( i => String( i ).replace( /\{id}/g, id ) )
		};
	}

	const rendered = String( patterns ).replace( /\{id}/g, id );

	return {
		id: rendered,
		ids: [rendered],
	};
}
