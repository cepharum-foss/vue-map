import { onBeforeUnmount, onMounted, ref, shallowRef, watchEffect } from "vue";

/**
 * Manages to attach some consuming component to the API of its containing map.
 *
 * @param {function} setup gets invoked when map has basically loaded
 * @returns {Attachable} exposes API for getting attached to a containing map's API
 */
export function useAttachable( setup ) {
	const me = shallowRef( null );

	const map = shallowRef( null );

	const ready = ref( false );

	onMounted( () => {
		// emit event to request consuming component's attachment to the map
		me.value.dispatchEvent( new CustomEvent( "vmAttach" ) );
	} );

	onBeforeUnmount( () => {
		// emit event to request consuming component's detaching from the map
		me.value.dispatchEvent( new CustomEvent( "vmDetach" ) );
	} );

	const attach = event => {
		if ( !event?.vueMapApi ) {
			throw new Error( 'invalid or missing event in vmAttach handler, make sure to use `@vmAttach="attach($event)"`' );
		}

		// wait for the provided map API reference to be actually available
		const mapApi = event.vueMapApi;
		const unwatch = watchEffect( () => {
			if ( mapApi.value ) {
				unwatch();

				map.value = mapApi.value;

				if ( typeof setup === "function" ) {
					// defer provided setup function until the map has loaded (its style)
					map.value.once( "load", () => Promise.resolve( setup() ).then( () => { ready.value = true; } ) );
				}
			}
		} );
	};

	return {
		me,
		map,
		ready,
		attach,
	};
}
