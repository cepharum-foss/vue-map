import { onMounted, onUnmounted, shallowRef } from "vue";
import { useMapStore } from "@/stores/map";
import { Marker } from "maplibre-gl";

const Axes = {
	bottom: [ 0, 1 ],
	right: [ 1, 0 ],
	top: [ 0, -1 ],
	left: [ -1, 0 ],
};

const Corners = [ [ 1, -1 ], [ 1, 1 ], [ -1, 1 ], [ -1, -1 ] ];

const Corner = {
	bottom: 0,
	right: 1,
	top: 2,
	left: 3,
};

/**
 * Implements parts of API common to components representing a marker on the
 * map.
 *
 * @param {Object<string,any>} props actual properties of a component consuming this API
 * @param {number} rotation clockwise rotation of marker in degrees
 * @param {[number, number]} offset PointLike offset of marker's center, negative offset move left/up
 * @returns {Object} partial API
 */
export function useMapMarker( props, rotation = 0, offset = undefined ) {
	/**
	 * Fetches Pinia store of named map to interact with it via its manager's
	 * API.
	 *
	 * @type {Cepharum.VueMap.MapState}
	 */
	const map = useMapStore( props.mapName );

	/**
	 * Reactively tracks reference to HTML element representing marker. It
	 * should be used as such by any component implementing the marker.
	 *
	 * @type {Ref<HTMLElement>}
	 */
	const element = shallowRef( null );

	/**
	 * Reactively tracks API of marker in context of its containing map.
	 *
	 * @type {Ref<Marker>}
	 */
	const api = shallowRef( null );    // eslint-disable-line consistent-this

	onMounted( () => {
		if ( !offset && rotation % 90 !== 0 ) {
			// calculate offset automatically based on the assumption that a
			// rectangular object gets rotated and one of its corners must match
			// the defined anchor point
			const angle = ( rotation + 360 ) % 360;
			const radiant = angle / 180 * Math.PI;
			const quadrant = Math.floor( angle / 90 );
			const corner = Corners[( Corner[props.anchor] + quadrant ) % Corners.length];
			const axis = Axes[props.anchor];

			if ( corner && axis ) {
				const rotatedCorner = [
					( corner[0] * Math.cos( -radiant ) ) - ( corner[1] * Math.sin( -radiant ) ),
					( corner[0] * Math.sin( -radiant ) ) + ( corner[1] * Math.cos( -radiant ) ),
				];

				const style = window.getComputedStyle( element.value );
				const width = parseFloat( style.width );
				const height = parseFloat( style.height );

				offset = [ // eslint-disable-line no-param-reassign
					axis[0] * ( rotatedCorner[0] - corner[0] ) * 0.5 * width,
					axis[1] * ( rotatedCorner[1] - corner[1] ) * 0.5 * height,
				];
			}
		}

		api.value = new Marker( {
			element: element.value,
			anchor: props.anchor || "bottom",
			rotation,
			offset: offset ?? [ 0, 0 ],
		} )
			.setLngLat( [ props.lng, props.lat ] )
			.addTo( map.api );
	} );

	onUnmounted( () => {
		api.value?.remove();
		api.value = undefined;

		element.value = undefined;
	} );

	/**
	 * Centers currently visible part of map at this marker's position using
	 * some animation.
	 *
	 * @returns {void}
	 */
	const center = () => {
		map.api.flyTo( {
			center: [ props.lng, props.lat ],
			animate: true,
			essential: true,
			linear: true,
		} );
	};

	return {
		api,
		map: map.api,

		element,

		center,
	};
}

/**
 * Provides definition of common properties for map marker components built with
 * partial API provided by useMapMarker().
 *
 * @type {Object}
 */
export const propsDefinition = {
	id: {
		type: [ String, Number ],
		required: true
	},
	lng: {
		type: Number,
		required: true
	},
	lat: {
		type: Number,
		required: true
	},
	mapName: {
		type: String,
		default: "map",
	},
	anchor: {
		type: String,
		default: "bottom",
	},
};
