import { ref, shallowRef, watchEffect } from "vue";

/**
 * Reads (and optionally monitors) curren geolocation of user.
 *
 * @param {{watch?: boolean, accurate?: boolean}} props property values of consuming component
 * @param {(error:Error) => void} error callback invoked in case of error
 * @returns {{position: Ref<[Number, Number] | null>, outdated: Ref<boolean>}} reactive position of user
 */
export function useGeolocation( props, error ) {
	const position = shallowRef( null );

	const outdated = ref( true );

	if ( navigator.geolocation ) {
		const onPositionUpdate = pos => {
			const lng = pos?.coords?.longitude;
			const lat = pos?.coords?.latitude;

			if ( lng && lat ) {
				position.value = [ lng, lat ];
				outdated.value = false;
			} else {
				outdated.value = true;
			}
		};

		const onPositionFailed = cause => {
			console.warn( "position error", cause );
			outdated.value = true;
			error( new Error( `position not available: ${cause.message}` ) );
		};

		let watcherId;

		watchEffect( () => {
			navigator.geolocation.clearWatch( watcherId );

			const options = {
				enableHighAccuracy: Boolean( props.accurate ),
				timeout: 10000,
				maximumAge: 60000,
			};

			if ( props.watch ) {
				watcherId = navigator.geolocation.watchPosition( onPositionUpdate, onPositionFailed, options );
			} else {
				navigator.geolocation.getCurrentPosition( onPositionUpdate, onPositionFailed, options );
			}
		} );
	} else {
		error( new Error( "browser lacks geolocation support" ) );
	}

	return {
		position,
		outdated,
	};
}
