import {computed, readonly, ref, shallowRef, watch} from "vue";
import { defineStore } from "pinia";
import { useDebounce } from "@/composables/debounce";
import ClusterWorker from "../workers/cluster.js?worker&inline";

/**
 * Defines delays to apply _in reverse order_ as part of a back-off strategy.
 *
 * @type {number[]}
 */
const backOffDelays = [ 500, 300, 200, 100, 100, 50, 50 ];

/**
 * Provides named store for managing state of a map.
 *
 * @param {string} mapName name of store, default is "map"
 * @returns {MapState} API of store managing state of a single map
 */
export const useMapStore = ( mapName = "map" ) => defineStore( mapName || "map", () => {
	/**
	 * Refers to running worker thread to interact with.
	 *
	 * @type {Worker}
	 */
	let worker;

	/**
	 * Tracks most recently used query ID for sending message to worker thread.
	 *
	 * @type {number}
	 */
	let workerQueryId = 0;

	/**
	 * Tracks pending queries send to worker thread meant to deliver according
	 * response send by worker in return.
	 *
	 * This is a map of query IDs into functions to invoke with a received
	 * result to resolve some promise returned to querying code before.
	 *
	 * @type {Map<number, function>}
	 */
	const workerQueries = new Map();

	/**
	 * Caches the latest set of parameters sent to worker thread for fetching
	 * (probably clustered) markers in a given excerpt of map.
	 *
	 * This is used to check if a returned set of markers is still matching
	 * latest state of map or can be ignored as a too late result.
	 *
	 * @type {{bounds: [number, number, number, number], zoom: number}}
	 */
	let fetchParameters;

	/**
	 * Reactively tracks API reference of map to use.
	 *
	 * @type {ref<Map>}
	 */
	const map = shallowRef( null );

	/**
	 * Lists all available markers by mapping either marker's ID into its
	 * instance.
	 *
	 * @note This map isn't reactive by intention to support addition of
	 *       multiple markers without each addition causing instant effect.
	 *       `revision` is used to trigger those instead.
	 *
	 * @type {Map<number, [number, number, number, object]>}
	 */
	const markers = new Map();

	/**
	 * Represents revision of available markers.
	 *
	 * When this value is changing, the set of markers has been adjusted and is
	 * ready for being processed.
	 *
	 * @type {Ref<number>}
	 */
	const revision = ref( 0 );

	/**
	 * Promises some pending request for clustering current set of markers has
	 * finished.
	 *
	 * @type {Ref<Promise<void>>}
	 */
	const whenClustered = shallowRef( Object.assign( Promise.resolve(), { $onSuccess: () => {} } ) ); // eslint-disable-line no-empty-function

	/**
	 * Caches stage of re-clustering some updated set of markers.
	 *
	 * - Stage 0 indicates, that no markers are currently re-clustered in a
	 *   separate worker thread.
	 * - Stage 1 indicates, that some updated set of markers has been sent to
	 *   worker thread for re-clustering.
	 * - Stage 2 indicates, that the worker thread has reported back on having
	 *   re-clustered provided markers successfully. In this stage a follow-up
	 *   retrieval of clustered markers for the most recently reported part of
	 *   map is triggered. It ends with those markers being delivered by the
	 *   worker thread.
	 *
	 * @type {Ref<number>}
	 */
	const clusteringStage = ref( 0 );

	/**
	 * Tracks timer used to delay another request for re-clustering updated set
	 * of markers while a previous request is still pending.
	 */
	let clusteringTimer;

	/**
	 * Controls delay applied next time when another request for re-clustering
	 * updated markers is triggered while a previous request is still in
	 * progress.
	 *
	 * @type {number}
	 */
	let clusteringBackOff = backOffDelays.length;

	/**
	 * Lists currently visible markers incl. cluster markers.
	 */
	const visibleMarkers = shallowRef( [] );

	const markerFilter = shallowRef( null );

	const markerFilterState = ref( {} );

	const moving = ref( 0 );

	/**
	 * Reduces precision of provided value which is expected to be a longitude
	 * or latitude value to 7 fractional digits.
	 *
	 * The reduction is required internally to safely map a provided value into
	 * an unsigned 32-bit integer on transferring positions to the worker thread.
	 *
	 * @param {number} value latitude or longitude value to reduce in precision
	 * @param {number} scale additional reduction to apply, defaults to 1, higher values additionally reduce precision
	 * @returns {number} provided value with precision reduced to 7 fractional digits
	 */
	const round = ( value, scale = 1 ) => Math.round( Number( value ) * scale * 10000000 ) / scale / 10000000;

	/**
	 * Triggers re-clustering process of updated markers and returns a promise
	 * that is resolved when this process has been finished.
	 *
	 * @returns {Promise<void>} promise for having re-clustered updated markers
	 */
	const onUpdatedMarkers = () => {
		revision.value++;

		const previous = whenClustered.value;
		let onSuccess;

		whenClustered.value = Object.assign( new Promise( resolve => {
			onSuccess = resolve;
		} ), { $onSuccess: onSuccess } );

		// prevent previous promise from pending when this new request's promise
		// has been settled
		whenClustered.value.then( previous, previous ); // eslint-disable-line promise/catch-or-return

		return whenClustered.value;
	};

	const getCurrentMetrics = () => {
		if ( !map.value ) {
			return null;
		}

		const bounds = map.value.getBounds();
		const zoom = map.value.getZoom();

		return {
			bounds: [
				round( bounds.getWest() ),
				round( bounds.getSouth() ),
				round( bounds.getEast() ),
				round( bounds.getNorth() ),
			],
			zoom,
		};
	};

	/**
	 * Creates worker thread for preparing clusters of markers in background.
	 *
	 * @returns {Promise<void>} promise for running worker
	 */
	const startWorker = () => {
		if ( typeof Worker !== "undefined" ) {
			if ( worker == null ) {
				worker = new ClusterWorker();
				worker.addEventListener( "message", onWorkerResponse );
			}
		}
	};

	/**
	 * Shuts down any recently started web worker.
	 *
	 * @returns {void}
	 */
	const stopWorker = () => {
		if ( worker ) {
			worker.removeEventListener( "message", onWorkerResponse );
			worker.terminate();

			worker = null;
		}
	};

	/**
	 * Responds to messages sent by worker thread.
	 *
	 * @param {MessageEvent} event message sent by worker thread
	 * @returns {void}
	 */
	const onWorkerResponse = event => {
		switch ( event.data?.name ) {
			case "clustered" :
				// some previously triggered re-clustering has been finished
				clusteringStage.value = 2;

				// make sure markers are actually re-fetched
				fetchParameters = null;

				fetchMarkers.trigger();
				break;

			case "markers" : {
				// some previously requested set of markers is available now
				if ( !fetchParameters || fetchParameters.received ) {
					console.warn( "@cepharum/vue-map: unexpected fetch result", event.data?.id );
					break;
				}

				const current = getCurrentMetrics();

				if ( current.zoom !== fetchParameters.zoom ||
				     current.bounds[0] !== fetchParameters.bounds[0] ||
				     current.bounds[1] !== fetchParameters.bounds[1] ||
				     current.bounds[2] !== fetchParameters.bounds[2] ||
				     current.bounds[3] !== fetchParameters.bounds[3] ) {
					console.warn( "@cepharum/vue-map: obsolete fetch result", event.data?.id );
					break;
				}

				if ( event.data?.id < workerQueryId ) {
					console.warn( "@cepharum/vue-map: ignoring late fetch result", event.data?.id );
					break;
				}

				fetchParameters.received = true;

				if ( clusteringStage.value === 2 ) {
					// after re-clustering markers had been re-fetched
					// -> those markers have been received now
					// -> mark re-clustering as done eventually
					clusteringStage.value = 0;

					whenClustered.value.$onSuccess();
				}

				const fetched = event.data.markers || [];
				const numFetched = Math.floor( fetched.length / 4 );
				const dst = new Array( numFetched );

				for ( let read = 0, write = 0; write < numFetched; write++ ) {
					const id = fetched[read++];
					const lng = ( fetched[read++] - 0x80000000 ) / 10000000;
					const lat = ( fetched[read++] - 0x80000000 ) / 10000000;
					const count = fetched[read++];

					if ( count > 0 ) {
						// cluster marker representing multiple regular markers
						let label;

						if ( count > 1000 ) {
							label = ( Math.round( count / 100 ) / 10 ) + "k";
						} else if ( count > 1000000 ) {
							label = ( Math.round( count / 100000 ) / 10 ) + "M";
						} else {
							label = String( count );
						}

						dst[write] = {
							type: "Feature",
							id,
							geometry: {
								type: "Point",
								coordinates: [ lng, lat ],
							},
							properties: {
								count,
								label,
								type: "cluster",
								cluster: true,
							},
						};
					} else {
						// regular marker
						const marker = markers.get( id );

						if ( !marker ) {
							console.warn( "marker clustering reported obsolete marker with ID", id );
							continue;
						}

						dst[write] = {
							type: "Feature",
							id,
							geometry: {
								type: "Point",
								coordinates: [ lng, lat ],
							},
							properties: marker[3],
						};
					}
				}

				visibleMarkers.value = dst;
				break;
			}

			default : {
				const respond = workerQueries.get( event.data?.id );

				if ( respond instanceof Function ) {
					workerQueries.delete( event.data.id );

					respond( event.data );
				}
			}
		}
	};

	/**
	 * Queries worker thread for set of (probably clustered) markers to show in
	 * current excerpt of map as tracked in reactive properties of this store.
	 *
	 * This is a debounced function, thus represented by an object consisting
	 * of a trigger() method to invoke as desired and a reactive property named
	 * `pending` which is true while a debouncing is taking place. Actual
	 * function isn't invoked if trigger() method gets invoked too often. During
	 * this time, `pending` is truthy.
	 *
	 * @type {{pending: ComputedRef<boolean>, trigger: function(): void}}
	 */
	const fetchMarkers = useDebounce( () => {
		if ( !map.value || !worker ) {
			return;
		}

		const current = getCurrentMetrics();
		if ( !current ) {
			return;
		}

		if ( fetchParameters &&
		     current.bounds[0] === fetchParameters.bounds[0] &&
		     current.bounds[1] === fetchParameters.bounds[1] &&
		     current.bounds[2] === fetchParameters.bounds[2] &&
		     current.bounds[3] === fetchParameters.bounds[3] &&
		     current.zoom === fetchParameters.zoom
		) {
			return;
		}

		fetchParameters = current;

		worker?.postMessage( {
			name: "fetch-cluster",
			id: ++workerQueryId,
			bounds: [...fetchParameters.bounds],
			zoom: fetchParameters.zoom,
		} );
	}, 100 );

	const onMoving = event => {
		moving.value |= event.type === "zoomstart" ? 1 : 2;
	};

	const onMoved = event => {
		moving.value &= ~( event.type === "zoomend" ? 1 : 2 );
	};

	/**
	 * Requests to start presentation of a map e.g. by loading according
	 * component.
	 *
	 * @param {Map} mapApi API of map rendering as implemented by maplibre-gl
	 * @return {void}
	 */
	const onMapPresentationStart = mapApi => {
		if ( map.value ) {
			throw Error( `map named "${mapName}" has been started before` );
		}

		map.value = mapApi;

		map.value.on( "zoomend", fetchMarkers.trigger );
		map.value.on( "moveend", fetchMarkers.trigger );

		fetchMarkers.trigger();

		map.value.on( "zoomstart", onMoving );
		map.value.on( "zoomend", onMoved );
		map.value.on( "movestart", onMoving );
		map.value.on( "moveend", onMoved );
	};

	/**
	 * Requests to stop presentation of a map e.g. by unloading according
	 * component.
	 *
	 * @return {void}
	 */
	const onMapPresentationStop = () => {
		if ( !map.value ) {
			throw Error( `map named "${mapName}" has been stopped before` );
		}

		map.value.off( "zoomend", fetchMarkers.trigger );
		map.value.off( "moveend", fetchMarkers.trigger );

		map.value.off( "zoomstart", onMoving );
		map.value.off( "zoomend", onMoved );
		map.value.off( "movestart", onMoving );
		map.value.off( "moveend", onMoved );

		fetchParameters = null;
		visibleMarkers.value = [];

		clearTimeout( clusteringTimer );
		clusteringStage.value = 0;
		whenClustered.value.$onSuccess();

		stopWorker();

		map.value = undefined;
	};

	/**
	 * Sends options of super cluster to be applied.
	 *
	 * @param {{radius: Number, minPoints: Number, maxZoom: number}} options options to be applied
	 * @returns {Promise<void>} promise resolved when options have been applied
	 */
	const configureCluster = async options => {
		await startWorker();

		worker?.postMessage( { name: "configure", ...options } );
	};

	/**
	 * Sends (updated) set of markers for clustering to worker thread unless a
	 * previously send chunk hasn't been clustered, yet.
	 *
	 * @returns {void}
	 */
	const sendMarkers = () => {
		if ( clusteringStage.value > 0 ) {
			clearTimeout( clusteringTimer );

			clusteringTimer = setTimeout( sendMarkers, backOffDelays[clusteringBackOff ? --clusteringBackOff : 0] );

			return;
		}

		clusteringBackOff = backOffDelays.length;

		if ( worker ) {
			const dst = new Uint32Array( markers.size * 3 );
			let write = 0;
			const options = typeof markerFilter.value === "function" ? markerFilterState.value ?? {} : null;

			for ( const marker of markers.values() ) {
				const [ id, lng, lat, properties ] = marker;

				if ( options && !markerFilter.value( options, id, lng, lat, properties ) ) {
					continue;
				}

				dst[write++] = id;
				dst[write++] = lng;
				dst[write++] = lat;
			}

			clusteringStage.value = 1;

			worker?.postMessage( { name: "restart", markers: dst }, [dst.buffer] );
		}
	};

	/**
	 * Queries provided message to worker and provides its response when
	 * available.
	 *
	 * @param {object} msg message to send
	 * @param {Array} transfers lists elements of `msg` to transfer to worker
	 * @returns {Promise<object>} promises worker's response
	 */
	const queryWorker = ( msg, transfers = [] ) => {
		if ( !msg ) {
			throw new TypeError( "nullish worker query rejected" );
		}

		if ( !worker ) {
			throw new TypeError( "nullish worker query rejected" );
		}

		if ( msg.id ) {
			if ( workerQueries.has( msg.id ) ) {
				throw new TypeError( `double use of message ID #${msg.id} in a query` );
			}
		} else {
			msg.id = ++workerQueryId; // eslint-disable-line no-param-reassign
		}

		let response;

		const result = new Promise( resolve => {
			response = resolve;
		} );

		workerQueries.set( msg.id, response );

		worker?.postMessage( msg, transfers );

		return result;
	};

	/**
	 * Requests to prepare data for clustering current set of markers using a
	 * worker thread.
	 *
	 * This is a debounced function, thus represented by an object consisting
	 * of a trigger() method to invoke as desired and a reactive property named
	 * `pending` which is true while a debouncing is taking place. Actual
	 * function isn't invoked if trigger() method gets invoked too often. During
	 * this time, `pending` is truthy.
	 *
	 * @type {{pending: ComputedRef<boolean>, trigger: function(): void}}
	 */
	const restartCluster = useDebounce( () => {
		// stopWorker();
		fetchParameters = null;
		startWorker();
		sendMarkers();
	}, 200 );

	watch( revision, restartCluster.trigger );

	// re-cluster markers when either filtering callback or its state's properties are changing
	watch( markerFilter, onUpdatedMarkers );
	watch( markerFilterState, onUpdatedMarkers, { deep: true } );

	/**
	 * Adds marker to the map.
	 *
	 * @param {string|number} id unique ID of marker to add
	 * @param {number} lng longitude of marker position
	 * @param {number} lat latitude of marker position
	 * @param {string} label label to show in marker
	 * @param {string} type type of marker
	 * @param {any[]} args additional properties of marker to add
	 * @returns {Promise<void>} promise for marker has been added successfully
	 */
	const addMarker = ( id, lng, lat, label, type = "default", ...args ) => {
		if ( !/^\d+$/.test( id ) || id < 1 || id > 0xffffffff ) {
			throw new TypeError( "ID must be positive unsigned 32bit integer" );
		}

		const _id = parseInt( id, 10 );
		const _lng = parseFloat( lng );
		const _lat = parseFloat( lat );

		if ( !( _lng >= -180 && _lng <= 180 ) || !( _lat >= -90 || _lat <= 90 ) ) {
			throw new TypeError( `invalid marker position ${_lng},${_lat} given as ${lng},${lat}` );
		}

		markers.set( _id, [
			_id,
			Math.round( _lng * 10000000 ) + 0x80000000,
			Math.round( _lat * 10000000 ) + 0x80000000,
			{ label, type, args },
		] );

		return onUpdatedMarkers();
	};

	/**
	 * Adds multiple markers to the map at once.
	 *
	 * @note Use this method instead of addMarker() whenever two or more markers
	 *       are added to improve map rendering performances.
	 *
	 * @param {Array<MarkerInfo>} newMarkers list of markers to add
	 * @param {boolean} reset if true, all existing markers get removed prior to adding new ones
	 * @returns {Promise<void>} promise for marker has been added successfully
	 */
	const addMarkers = async( newMarkers, { reset = false } = {} ) => {
		let updated = reset && markers.size > 0;

		if ( reset ) {
			markers.clear();
		}

		for ( const marker of newMarkers ) {
			const { id, lng, lat } = marker;

			if ( !/^\d+$/.test( id ) || id < 1 || id > 0xffffffff ) {
				console.error( "@cepharum/vue-map: ignoring marker with invalid ID", id );
				continue;
			}

			const _id = parseInt( id, 10 );
			const details = {};

			for ( const name of Object.keys( marker ) ) {
				switch ( name ) {
					case "id" :
					case "lng" :
					case "lat" :
						break;

					default :
						details[name] = marker[name];
				}
			}

			markers.set( _id, [
				_id,
				Math.round( lng * 10000000 ) + 0x80000000,
				Math.round( lat * 10000000 ) + 0x80000000,
				details,
			] );

			updated = true;
		}

		if ( updated ) {
			await onUpdatedMarkers();
		}
	};

	/**
	 * Removes previously added markers each selected by its ID as given on
	 * adding it.
	 *
	 * @param {string|number} ids IDs of markers to remove
	 * @returns {void}
	 */
	const removeMarkers = async( ...ids ) => {
		let updated = false;

		for ( const id of ids ) {
			if ( /^\d+$/.test( id ) ) {
				const _id = parseInt( id, 10 );

				if ( markers.has( _id ) ) {
					markers.delete( _id );

					updated = true;
				}
			}
		}

		if ( updated ) {
			await onUpdatedMarkers();
		}
	};

	/**
	 * Removes all previously added markers.
	 *
	 * @returns {void}
	 */
	const removeAllMarkers = async() => {
		if ( markers.size > 0 ) {
			markers.clear();
			await onUpdatedMarkers();
		}
	};

	/**
	 * Fetches previously added marker selected by its numeric ID.
	 *
	 * @param {number} id unique numeric ID of marker to fetch
	 * @returns {MarkerInfo} found marker, null if marker wasn't found
	 */
	const getMarkerById = id => {
		const record = markers.get( id );
		if ( record == null ) {
			return null;
		}

		const [ , lng, lat, details ] = record;

		return {
			id,
			lng: ( lng - 0x80000000 ) / 10000000,
			lat: ( lat - 0x80000000 ) / 10000000,
			...details,
		};
	};

	/**
	 * Searches marker picked by provided callback.
	 *
	 * @param {function(marker:MarkerInfo):boolean} fn callback invoked on either marker to picked the searched one
	 * @returns {MarkerInfo} found marker, null if marker wasn't found
	 */
	const findMarker = fn => {
		for ( const [ id, record ] of markers.entries() ) {
			const [ , lng, lat, details ] = record;
			const marker = {
				id,
				lng: ( lng - 0x80000000 ) / 10000000,
				lat: ( lat - 0x80000000 ) / 10000000,
				...details,
			};

			if ( fn( marker ) ) {
				return marker;
			}
		}

		return null;
	};

	const flyTo = ( zoom, lng, lat ) => {
		if ( !map.value ) {
			return;
		}

		const info = {};

		const isAtMinZoom = map.value.getZoom() === map.value.getMinZoom();

		if ( !isAtMinZoom && !isNaN( Number( zoom ) ) ) {
			info.zoom = Number( zoom );
		}

		if ( !isNaN( Number( lng ) ) && !isNaN( Number( lat ) ) ) {
			const roundedLng = round( lng, 100 );
			const roundedLat = round( lat, 100 );
			const center = map.value.getCenter();

			if ( roundedLng !== round( center.lng, 100 ) || roundedLat !== round( center.lat, 100 ) ) {
				info.center = {
					lng: round( lng ),
					lat: round( lat ),
				};
			}
		}

		map.value[isAtMinZoom ? "easeTo" : "flyTo"]( info );
	};

	return {
		// expose map's manager API
		api: computed( () => map.value ),

		// expose custom helper functions
		configureCluster,
		flyTo,
		round,

		// expose basic properties required for rendering map and for responding to interactions
		markers: computed( () => visibleMarkers.value ),

		// provide additional information on current state of map
		isMoving: computed( () => moving.value !== 0 ),
		isClustering: computed( () => clusteringStage.value > 0 ),
		whenClustered,

		// expose callback and state tracked for changes to reactively limit
		// visible markers
		markerFilter,
		markerFilterState,

		// actions for controlling time of presenting the map
		onMapPresentationStart,
		onMapPresentationStop,

		// expose methods for adjusting markers
		addMarker,
		addMarkers,
		removeMarkers,
		removeAllMarkers,
		revision: computed( () => revision.value ),

		getMarkerById,
		findMarker,

		/**
		 * Queries worker thread for zoom level at which selected cluster gets
		 * expanded into its children.
		 *
		 * @param {number} markerId ID of a cluster marker
		 * @returns {Promise<number>} promises zoom level at which selected cluster marker gets expanded
		 */
		getClusterExpansionZoom: markerId => queryWorker( {
			name: "getClusterExpansionZoom",
			marker: markerId,
		} ).then( result => result.zoom ),

		/**
		 * Queries worker thread for regular and cluster markers subordinated to
		 * cluster marker selected by its ID.
		 *
		 * @param {number} markerId ID of a cluster marker
		 * @returns {Promise<GeoJSON.Feature[]>} promises list of markers
		 */
		getClusterChildren: markerId => queryWorker( {
			name: "getClusterChildren",
			marker: markerId,
		} ).then( result => result.children ),

		/**
		 * Queries worker thread for (an excerpt of) regular markers that are
		 * represented by the selected cluster marker.
		 *
		 * @param {number} markerId ID of a cluster marker
		 * @param {number} limit number of regular markers to fetch
		 * @param {number} offset index of first regular marker to fetch
		 * @returns {Promise<GeoJSON.Feature[]>} promises list of regular markers
		 */
		getClusterLeaves: ( markerId, limit = Infinity, offset = 0 ) => queryWorker( {
			name: "getClusterLeaves",
			marker: markerId,
			limit,
			offset,
		} ).then( result => result.leaves ),
	};
} )();
