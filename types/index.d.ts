import {Ref, ShallowRef} from "vue";
import {default as MapLibre, GeoJSONFeature, Map as MapAPI, Marker} from "maplibre-gl";

declare namespace Cepharum.VueMap {
	/**
	 * Exposes whole MapLibreGL library that's been used internally.
	 */
	export type MapLibre = typeof MapLibre;

	/**
	 * Describes state of a map.
	 */
	export interface MapState {
		/**
		 * Exposes API of map manager mostly for internal use such as actually
		 * attaching markers and layers to it.
		 *
		 * Currently, don't use it e.g. for navigating the map as changes made
		 * here aren't tracked reactively and thus may cause presented map being
		 * out of sync.
		 *
		 * @internal
		 */
		api: MapAPI;

		/**
		 * Animates change of visible map excerpt to center at given position
		 * and zoom level.
		 *
		 * @note Prefer this method over directly interacting with the map's API
		 *       for this method is in sync with position tracked in store to
		 *       control when to fetch what markers.
		 *
		 * Omit zoom or longitude and latitude if you want to keep either
		 * information as it is.
		 *
		 * @param zoom new zoom level, omit for keeping current zoom level
		 * @param longitude longitude of new center position, omit together with latitude to keep map's center
		 * @param latitude latitude of new center position, omit together with longitude to keep map's center
		 */
		flyTo( zoom?: Number, longitude?: Number, latitude?: Number ): void;

		/**
		 * Rounds provided longitude/latitude value according to store-internal
		 * precision. This is used to prevent slight deviations causing
		 * unnecessary re-processings and re-renderings.
		 *
		 * You don't have to use this function for most API methods apply it
		 * internally. But if you happen to work with map's API directly, you
		 * might need to use it to prevent odd behaviors.
		 *
		 * @param value value to be rounded
		 * @param scale additional scale applied, defaults to 1, higher values reduce precision of resulting value
		 */
		round( value: Number, scale?: Number ): Number;

		/**
		 * Adjusts options of marker cluster to be considered on re-clustering
		 * markers next time e.g. after adding/removing markers.
		 *
		 * @param options set of options to adjust, omitted options are kept unchanged
		 *
		 */
		configureCluster( options: { radius?: Number, minPoints?: Number, maxZoom?: Number } ): Promise<void>;

		/**
		 * Exposes currently visible markers.
		 */
		markers: Array<ClusteredPOI|SinglePOI>

		/**
		 * Indicates if map is currently re-clustering.
		 *
		 * The map is re-clustering each time the set of basically available
		 * markers is updated. Based on number of markers, this re-clustering
		 * may take a significant time of up to several seconds e.g. with
		 * several 100k markers.
		 *
		 * Marker interaction should be limited while map is re-clustering.
		 */
		isClustering: boolean;

		/**
		 * Indicates if visible part of map is currently changing due to
		 * panning, zooming etc.
		 */
		isMoving: boolean;

		/**
		 * Promises any pending re-clustering of markers being done.
		 */
		whenClustered: Promise<void>;

		/**
		 * This method is invoked by MapView when mounted. It is basically
		 * setting up the store and tracks the given map instance there e.g.
		 * for sharing with marker components. In addition, a worker thread is
		 * created to calculate clusters of markers in background.
		 *
		 * @note This method is part of low-level interactions between
		 *       components and should be used internally, only.
		 *
		 * @internal
		 * @param map
		 */
		onMapPresentationStart( map ): void;

		/**
		 * This method is the counterpart to onMapPresentationStart() and its
		 * releasing any resources allocated in that method such as the separate
		 * worker thread.
		 *
		 * @note This method is part of MapView component and shouldn't be used.
		 *
		 * @internal
		 */
		onMapPresentationStop(): void;

		/**
		 * Adds marker of selected type at specified position on map.
		 *
		 * @note This method instantly updates set of markers and thus is going
		 *       to trigger a re-clustering of map markers that might take a
		 *       moment to complete.
		 *
		 *       Always use addMarkers() if you intend to add multiple markers
		 *       at once.
		 *
		 * @param id unique ID of marker
		 * @param lng longitude of marker's position on map
		 * @param lat latitude of marker's position on map
		 * @param label label to show on marker
		 * @param type type of marker, defaults to "default"
		 * @param args additional properties of marker to add
		 */
		addMarker( id: number, lng: number, lat: number, label: string, type?: string, ...args: any[] ): Promise<void>;

		/**
		 * Adds a list of markers to the map and triggers re-clustering
		 * afterward.
		 *
		 * @note Always prefer this method over addMarker() to prevent
		 *       unnecessary re-clusterings resulting in significant processing
		 *       loads and potential side effects such as reduced responsiveness.
		 *
		 * @param newMarkers list of marker descriptions, a shallow copy of every (additional) property is kept internally
		 * @param options customizes process of adding markers to the map
		 */
		addMarkers( newMarkers: MarkerInfo[], options?: AddMarkerOptions ): Promise<void>;

		/**
		 * Removes all markers each identified by its unique ID from map and
		 * triggers re-clustering of left markers afterward.
		 *
		 * @param ids IDs of markers to remove
		 */
		removeMarkers( ...ids: Array<number> ): Promise<void>;

		/**
		 * Removes all markers from map and triggers "re-clustering" afterward.
		 */
		removeAllMarkers(): Promise<void>;

		/**
		 * Exposes revision number of marker set. It changes each time marker(s)
		 * has/have been added or updated.
		 *
		 * Use this for responding to a change of markers.
		 */
		revision: number;

		/**
		 * Fetches previously added marker selected by its numeric ID.
		 *
		 * @param id unique numeric ID of marker to fetch
		 */
		getMarkerById( id: number ): MarkerInfo|null;

		/**
		 * Searches markers using some callback.
		 *
		 * @param callback function to invoke per marker to decide if it's the searched one
		 */
		findMarker( callback: ( info: MarkerInfo ) => boolean ): MarkerInfo|null;

		/**
		 * Reactive reference to callback function invoked per marker to decide
		 * whether either marker should be visible or not.
		 *
		 * When set, this filter is considered on every update of markers
		 * causing them to be re-clustered in a background thread.
		 *
		 * @param options exposes value of markerFilterState, don't change anything here to prevent repeated updates
		 * @param id unique integer ID of marker to check
		 * @param lng longitude of marker to check
		 * @param lat latitude of marker to check
		 * @param properties properties of marker, e.g. its `type` or (if available) its `label`
		 * @returns true if marker should be visible
		 */
		markerFilter?: ( options: { [key: string]: any }, id: number, lng: number, lat: number, properties: { [key: string]: any } ) => boolean;

		/**
		 * Provides custom set of properties tracking state of marker filter
		 * options to be considered by markerFilter callback function.
		 *
		 * The particular structure of this object depends on particular
		 * implementation of markerFilter function. The store is providing this
		 * object for watching it, only, to trigger update of markers whenever
		 * this set of options changes.
		 */
		markerFilterState: { [key: string]: any };

		/**
		 * Retrieves zoom level at which a cluster marker selected by its unique
		 * ID is replaced with markers it is representing.
		 *
		 * At resulting zoom level, there may be more cluster markers each
		 * representing another subset of regular markers.
		 *
		 * @param markerId ID of cluster marker to inspect
		 * @returns zoom level required to have all markers
		 */
		getClusterExpansionZoom( markerId: number ): Promise<number>;

		/**
		 * Retrieves markers a selected cluster marker is representing. This may
		 * include further cluster markers each representing another subset of
		 * markers.
		 *
		 * @param markerId unique ID of cluster marker to inspect
		 * @returns list of markers represented by selected cluster marker
		 */
		getClusterChildren( markerId: number ): Promise<GeoJSONFeature[]>;

		/**
		 * Retrieves all regular markers a selected cluster marker is
		 * representing. The resulting list does not include any further cluster
		 * markers but regular markers, only.
		 *
		 * @param markerId unique ID of cluster marker to inspect
		 * @returns list of regular markers represented by selected cluster marker
		 */
		getClusterLeaves( markerId: number ): Promise<GeoJSONFeature[]>;
	}

	/**
	 * Describes single marker in a list of markers to be added to a map.
	 *
	 * @see addMarkers()
	 */
	export interface MarkerInfo {
		/**
		 * Provides unique ID of marker.
		 */
		id: number;

		/**
		 * Provides longitude of marker's position.
		 */
		lng: number;

		/**
		 * Provides latitude of marker's position.
		 */
		lat: number;

		/**
		 * Provides label to show on marker. A label is required unless using
		 * custom markers.
		 */
		label?: string;

		/**
		 * Selects type of marker. Defaults to "default" when omitted.
		 */
		type?: string;
	}

	/**
	 * Describes custom options available on adding multiple markers to the map.
	 */
	export interface AddMarkerOptions {
		/**
		 * If true, all existing markers are removed prior to adding new ones.
		 * Otherwise, any set of markers added before is augmented with the new
		 * set of markers.
		 */
		reset?: boolean;
	}

	/**
	 * Describes special marker representing a whole cluster of regular markers.
	 */
	export interface ClusteredPOI extends POI {
		properties: {
			/**
			 * Provides number of regular markers represented by current cluster
			 * marker.
			 */
			count: number;

			/**
			 * Provides human-readable number of markers represented by current
			 * cluster marker optionally supporting suffixes according to metric
			 * system on larger numbers.
			 */
			label: string;

			/**
			 * Provides type of marker. Must be "cluster" for markers
			 * representing a whole cluster of regular markers.
			 */
			type: "cluster";

			/**
			 * Indicates current marker to represent a cluster of regular
			 * markers.
			 */
			cluster: true;
		}
	}

	/**
	 * Describes a regular marker representing a single point of interest on
	 * map.
	 */
	export interface SinglePOI extends POI {
		properties: {
			/**
			 * Provides label to show on marker.
			 */
			label: string;

			/**
			 * Provides type of marker. Can be used e.g. to show differently
			 * styled markers per type.
			 */
			type: string;
		}
	}

	/**
	 * Commonly describes a marker on the map similar to a GeoJSONFeature.
	 */
	export interface POI {
		/**
		 * Indicates this record to describe a GeoJSON feature.
		 */
		type: "Feature";

		/**
		 * Uniquely identifies this marker in context of current map.
		 */
		id: number;

		/**
		 * Provides position of marker on map.
		 */
		geometry: {
			/**
			 * Indicates this information to describe a GeoJSON point.
			 */
			type: "Point";

			/**
			 * Describes position of marker on map by its longitude and
			 * latitude.
			 */
			coordinates: [ number, number ];
		};

		/**
		 * Provides additional information on marker.
		 */
		properties: Object;
	}

	/**
	 * Fetches API of manager controlling named map.
	 *
	 * @param mapName name of Pinia store describing map's state, default is "map" when omitted
	 * @returns API for interacting with map
	 */
	export function useMapStore( mapName?: string ): MapState;

	/**
	 * Fetches API for interacting with single marker of a map.
	 *
	 * @param properties properties of a marker component
	 * @returns API for interacting with marker described by properties
	 */
	export function useMapMarker( properties: CommonMarkerProperties ): MarkerState;

	/**
	 * Defines properties to be commonly supported by components implementing a
	 * map marker.
	 */
	export interface CommonMarkerProperties {
		/**
		 * Uniquely identifies this marker in context of its containing map.
		 *
		 * ID must be non-zero unsigned 32-bit integer or some string
		 * representing such an integer.
		 */
		id: Number | String;

		/**
		 * Provides the longitude of marker's position on map.
		 */
		lng: Number;

		/**
		 * Provides the latitude of marker's position on map.
		 */
		lat: Number;

		/**
		 * Provides name of containing map's store.
		 *
		 * Default is "map" when omitted.
		 */
		mapName?: String;

		/**
		 * Names marker's anchor position.
		 *
		 * Default is "top-left" when omitted.
		 *
		 * @see This property is equivalent to `options.anchor` described at
		 *      https://maplibre.org/maplibre-gl-js-docs/api/markers/#marker.
		 */
		anchor?: String;
	}

	/**
	 * Provides API supporting in a marker component's implementation.
	 */
	export interface MarkerState {
		/**
		 * Exposes API of marker's manager.
		 */
		api: Ref<Marker>;

		/**
		 * Exposes instance of map API current marker is associated with.
		 */
		map: MapAPI;

		/**
		 * Exposes reactive reference to HTML element implementing this marker.
		 *
		 * Any component implementing that marker and consuming the marker's
		 * state has to use a ref attribute of same name pointing the HTML
		 * element. When mounting that component, this reference is used to
		 * actually register the marker with the map's API.
		 */
		element: Ref<HTMLElement>;

		/**
		 * Adjusts current view of map to display this marker in its center.
		 */
		center(): void;
	}

	/**
	 * Creates API for integrating calling component with a containing map.
	 *
	 * @param setup gets called on prepared map to customize it as needed
	 */
	export function useAttachable( setup: () => void ): Attachable;

	/**
	 * Gets emitted as DOM event targeting a component requesting to be
	 * integrated with a containing map. That map is expected to inject its API
	 * when _capturing_ this event.
	 */
	export interface VueMapAttachEvent extends CustomEvent {
		/**
		 * Refers to the API of containing map.
		 */
		vueMap: MapAPI;
	}

	/**
	 * Provides consumable API for attaching a component to a containing map.
	 */
	export interface Attachable {
		/**
		 * Refers element of consuming component.
		 *
		 * The consuming component must use `ref="me"` in its template.
		 */
		me: ShallowRef<HTMLElement>;

		/**
		 * Exposes API of containing map after successfully attaching to it.
		 */
		map: ShallowRef<MapAPI>;

		/**
		 * Indicates if setup handler has been processed after integrating with
		 * containing map.
		 */
		ready: Ref<boolean>;

		/**
		 * Exposes handler for the custom vmAttach event.
		 *
		 * The consuming component must use `@vmAttach="attach($event)"`.
		 *
		 * @param event event to be handled
		 */
		attach: (event: VueMapAttachEvent) => void;
	}
}
