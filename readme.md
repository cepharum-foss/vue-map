# @cepharum/vue-map

a Vue3 component library for presenting vector-based map

## License

[BSD 3-clause](LICENSE)

## About

This library implements several [Vue3](https://vuejs.org/) components for presenting a map with markers based on [MapLibre](https://maplibre.org/maplibre-gl-js-docs/api/) and [supercluster](https://www.npmjs.com/package/supercluster). Its API is utilizing a [Pinia](https://pinia.vuejs.org/)-based store for managing map's state and all map markers.

## Install

Install as dependency with

```sh
npm install @cepharum/vue-map
```

In addition, make sure that vue and pinia are installed as dependencies of your project:

```sh
npm install vue pinia
```

## Setup

You can integrate the library's map presenter rather easily:

```vue
<script setup>
import { MapPresenter } from "@cepharum/vue-map/dist/vue-map";
import "@cepharum/vue-map/dist/style.css";
</script>

<template>
	<MapPresenter
		:origin="{lng: 13.37770, lat: 52.51628}"
		map-style="https://your.tile.server/styles/basic-preview/style.json"
		zoom="12"
		min-zoom="8"
		max-zoom="19"
	/>
</template>
```

First import is selecting the presenter component. Second import is including all style definitions specific to this library (such as declaring some CSS properties for customizing the design) and to the underlying maplibre-gs library.

In the template, the presenter component is used. You need to provide the URL of your maptile server's style to use in attribute map-style. Providing origin is recommended. All other properties have reasonable defaults.
