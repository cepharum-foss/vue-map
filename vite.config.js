import { fileURLToPath, URL } from "node:url";

import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";

// https://vitejs.dev/config/
export default defineConfig( {
	plugins: [vue()],
	resolve: {
		alias: {
			"@": fileURLToPath( new URL( "./src", import.meta.url ) )
		}
	},
	build: {
		lib: {
			entry: fileURLToPath( new URL( "./src/index.js", import.meta.url ) ),
			name: "VueMap",
			fileName: "vue-map",
		},
		rollupOptions: {
			external: [ "vue", "pinia" ],
			output: {
				globals: {
					vue: "Vue",
					pinia: false,
				}
			}
		}
	}
} );
